# README: How to Run This Code
#
# Ensure that this file is placed in the same directory as the two .csv files.
# Execute this script in whatever environment you prefer, from any directory.
# If getting error such as "__file__ is not defined" (e.g. from Jupyter),
# replace line 13 by: `current_directory = '.'`, and execute the script 
# from the directory which contains this script as well as the data files.

import os
import csv
import matplotlib.pyplot as plt

current_directory = os.path.dirname(os.path.abspath(__file__))

# Utility function to add to an array
# which may contain None values.
def add_to(dictionary, key, amount):
	dictionary[key] = amount + dictionary.get(key, 0)

# Counts how many members have a certain home ownership
home_ownership_counts = {}

# Stores the home ownership of each member
member_home_ownership = {}

# Stores the total loan of all members with a certain home ownership
home_ownership_totals = {}

# Stores the average loan amount per home ownership
home_ownership_averages = {}

# Read home ownership data
with open(current_directory + '/home_ownership_data.csv') as loans_file:
	reader = csv.DictReader(loans_file)
	for row in reader:
		# Extract row information
		member_id = row['member_id']
		home_ownership_type = row['home_ownership']
		
		# Increment home ownership count and map to member
		member_home_ownership[member_id] = home_ownership_type
		add_to(home_ownership_counts, home_ownership_type, 1)

# Read loans data
with open(current_directory + '/loan_data.csv') as loans_file:
	reader = csv.DictReader(loans_file)
	for row in reader:
		# Extract row information
		member_id = row['member_id']
		loan_amount = int(row['loan_amnt'])
	
		# Add loan amount to totals
		home_ownership_type = member_home_ownership[member_id]
		add_to(home_ownership_totals, home_ownership_type, loan_amount)
		
# Compute averages
for home_ownership_type, members_count in home_ownership_counts.items():
	total_loans = home_ownership_totals[home_ownership_type]
	home_ownership_averages[home_ownership_type] = total_loans / members_count

# Display results
print('Average loan amount per home ownership type:\n')
for home_ownership_type, average_loans in home_ownership_averages.items():
	print("{:10}: {:8.2f}$".format(home_ownership_type, average_loans))
	
# Display graph

index = range(len(home_ownership_averages))

plt.figure(figsize=(9,6))
plt.title('Average Loan Amount per Home Ownership Type')
plt.xlabel('Home Ownership Type')
plt.ylabel('Average Loan Amount')
plt.bar(index, list(home_ownership_averages.values()), align='center')
plt.xticks(index, list(home_ownership_averages.keys()))
plt.show()